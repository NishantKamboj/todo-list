const inputBox = document.getElementById('task_box')
const haveButton = document.getElementById('haveButton')
const needButton = document.getElementById('needButton')
const haveList = document.getElementById('have_listContainer')
const needList = document.getElementById('need_listContainer')


const haveClick = () =>{
    if (inputBox.value === '') {
        alert('You must write somthing!');
        return;
    }
    let li = document.createElement('li');
    li.innerHTML = `<div>
        <label>
            <input type="checkbox" class="todoitem" value="${inputBox.value}" checked/>
            <span>${inputBox.value}</span>
        </label>
        <button class="removeButton">&times;</button>
        
    </div>`;
    haveList.appendChild(li);
    inputBox.value = '';  
    saveData()
   
}

const needClick = () =>{
    if (inputBox.value === '') {
        alert('You must write somthing!')
        return;
    }
    let li = document.createElement('li');
    li.innerHTML = `<div> 
        <label>
            <input type="checkbox" class="todoitem" value="${inputBox.value}"/>
            <span>${inputBox.value}</span>
        </label>       
        <button class="removeButton">&times;</button>
    </div>`
    needList.appendChild(li);
    inputBox.value = '';
    saveData()
    
};

document.addEventListener("click", function(e){
    const {target} = e;
    if(target.classList.contains('removeButton')) {
        target.closest('li').remove();
        saveData();
    }
 
  
    if(target.classList.contains('todoitem')) {
        const { value, checked } = target;
        const parent = target.closest('li');

        if(checked) {
            let li = document.createElement('li');
            li.innerHTML = `<div>
                <label>
                    <input type="checkbox" class="todoitem" value="${value}" checked/>
                    <span>${value}</span>
                </label>
                <button class="removeButton">&times;</button>
                
            </div>`;
            haveList.appendChild(li);
            parent.remove();
            saveData()
            return;                        
        }
        
        let li = document.createElement('li');
        li.innerHTML = `<div>
            <label>
                <input type="checkbox" class="todoitem" value="${value}"/>
                <span>${value}</span>
            </label>       
            <button class="removeButton">&times;</button>
        </div>`
        needList.appendChild(li);
        parent.remove();
        saveData()
        
    }

});

const saveData = () => {
    console.log(saveData)
    localStorage.setItem('data1', haveList.innerHTML );
    localStorage.setItem('data2', needList.innerHTML );
    
}
const showData = () =>{
    haveList.innerHTML = localStorage.getItem('data1')
    needList.innerHTML = localStorage.getItem('data2')
   
}
showData()  
 
